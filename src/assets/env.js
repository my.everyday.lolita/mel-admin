(function (window) {
  let domains = {
    registration: 'http://192.168.1.22:3000',
    login: 'http://auth.mel.localhost',
    mel: 'http://api.mel.localhost',
  };
  window["env"] = window["env"] || {
    domains
  };

})(this);
