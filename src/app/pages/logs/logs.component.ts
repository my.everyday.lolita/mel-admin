import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { Observable, Subject } from 'rxjs';
import { startWith, switchMap, takeUntil } from 'rxjs/operators';
import { rowAnimation } from 'src/app/features/animations/fix-mat-table-route-transition';
import { environment } from 'src/environments/environment';

export enum LogOrigin {
  FRONT = 'front',
  BACK = 'back'
}

export enum LogType {
  ERROR = 'error',
  WARNING = 'warning',
}

export interface Log {
  origin: LogOrigin;
  type: LogType;
  message: string;
  date: Date;
  trace: string;
  context: string;
}

@Component({
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.scss'],
  animations: [
    rowAnimation,
  ]
})
export class LogsComponent implements OnInit, OnDestroy {

  data: Log[] = [];
  displayedColumns = ['message', 'date', 'origin', 'type'];
  resultsLength = 0;
  pageSize = parseInt(localStorage.getItem('logs:pageSize') || '10', 10);
  pageSizeOptions = [10, 20, 50];

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild('detail', { static: true }) detailTemplate!: TemplateRef<any>;

  private unsubscriber = new Subject();

  constructor(
    private http: HttpClient,
    private dialog: MatDialog
  ) { }

  ngOnDestroy(): void {
    this.unsubscriber.next();
    this.unsubscriber.complete();
  }

  ngOnInit(): void {
    this.paginator.page.pipe(
      startWith({}),
      switchMap(() => {
        const limit = this.paginator.pageSize || this.pageSize;
        const skip = (this.paginator.pageIndex || 0) * limit;
        localStorage.setItem('logs:pageSize', limit.toString());
        return this.getLogs(skip, limit);
      }),
      takeUntil(this.unsubscriber)
    ).subscribe({
      next: response => {
        this.resultsLength = response.total;
        this.data = response.items || [];
      }
    });
  }

  showDetails(row: Log): void {
    this.dialog.open(this.detailTemplate, {
      data: row
    });
  }

  private getLogs(skip: number, limit: number): Observable<{ total: number, items: Log[] }> {
    return this.http.get<{ total: number, items: Log[] }>(`${environment.domains.mel}/api/logs`, {
      params: new HttpParams({
        fromObject: { skip: skip.toString(), limit: limit.toString() }
      }),
      headers: new HttpHeaders({
        Authorization: 'auto'
      })
    });
  }

}
