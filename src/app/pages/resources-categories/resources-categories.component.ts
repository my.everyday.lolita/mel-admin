import { NestedTreeControl } from '@angular/cdk/tree';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, AbstractControl } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { ActivatedRoute } from '@angular/router';
import { Category, ItemCategory, ResourcesCategoriesService, ItemStatsCollection } from '@my-everyday-lolita/mel-shared';
import { rowAnimation } from 'src/app/features/animations/fix-mat-table-route-transition';
import { EditableExpandableItem, Item } from 'src/app/features/form/form.model';
import { ResourcesItemsCountMixin } from 'src/app/features/resources/resources-items-count.mixin';
import { ResourcesListPageMixin } from 'src/app/features/resources/resources-list-page.mixin';
import { ResourcesListPage } from 'src/app/features/resources/resources-mixins.model';
import { ResourcesService } from 'src/app/features/resources/resources.service';

export class BrandResourcesListPage extends ResourcesListPage<Category> { }

@Component({
  templateUrl: './resources-categories.component.html',
  styleUrls: ['./resources-categories.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    rowAnimation,
  ]
})
export class ResourcesCategoriesComponent extends
  ResourcesItemsCountMixin<Category>(ResourcesListPageMixin<Category>(BrandResourcesListPage)) {

  displayedColumns: string[] = ['name', 'nbItems', 'actions'];
  pageSize = localStorage.getItem('pageSize') || 10;
  pageSizeOptions = [10, 20, 50];
  resources: Item<Category>[];
  dataSource!: MatTableDataSource<Item<Category>>;

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('addResourceDialog', { static: true }) addResourceTemplate!: TemplateRef<any>;

  constructor(
    private activatedRoute: ActivatedRoute,
    protected cdRef: ChangeDetectorRef,
    protected service: ResourcesCategoriesService,
    protected dialog: MatDialog,
    protected resourcesService: ResourcesService
  ) {
    super();

    this.resources = this.toEditableExpandableItem(JSON.parse(JSON.stringify(this.activatedRoute.snapshot.data.categories)));
  }

  buildForm(resource?: Category): FormGroup {
    const form = super.buildForm(resource);
    form.addControl('name', new FormControl(resource && resource.name || '', [Validators.required]));
    form.addControl('shortname', new FormControl(resource && resource.shortname || ''));
    form.addControl('children', new FormArray((resource && resource.children || []).map(child => {
      return this.createFormNode(child);
    })));
    if (resource && resource._id) {
      form.addControl('_id', new FormControl(resource._id || '', [Validators.required]));
    }
    return form;
  }

  onUpdate(item: EditableExpandableItem<Category>, response: Category, dialogRef?: MatDialogRef<TemplateRef<any>>): void {
    super.onUpdate(item, response);
    item.data = response;
    item.form.setValue({
      name: response.name,
      shortname: response.shortname,
      children: response.children,
      _id: response._id,
    });
    this.cdRef.detectChanges();
    if (dialogRef) {
      dialogRef.close();
    }
  }

  onCreate(item: EditableExpandableItem<Category>, response: Category, dialogRef?: MatDialogRef<TemplateRef<any>>): void {
    if (dialogRef) {
      dialogRef.close();
    }
  }

  openEditForm(element: EditableExpandableItem<Category>): void {
    this.dialog.open(this.addResourceTemplate, {
      data: {
        ...element,
        edit: true,
      },
      disableClose: true,
      minWidth: '50vw'
    });
  }

  createOrUpdate(data: { form: AbstractControl, edit?: boolean }, ref: MatDialogRef<any>): void {
    if (data.edit) {
      this.update(data as any, ref);
    } else {
      this.create(data as any, ref);
    }
  }

  makeEditableExpandableItem(item: Category): EditableExpandableItem<Category> {
    const wrapper = super.makeEditableExpandableItem(item);
    wrapper.dataSource = new MatTreeNestedDataSource();
    wrapper.dataSource.data = [wrapper.form];
    wrapper.treeControl = new NestedTreeControl<FormGroup>(node => {
      return node.controls.children && (node.controls.children as any).controls as any;
    });
    // Expand root by default.
    wrapper.treeControl.expand(wrapper.form);
    return wrapper;
  }

  hasChild(_: number, node: FormGroup): boolean {
    return node.controls.children !== undefined && node.value.children.length > 0;
  }

  makeEmptyItem(): Category {
    return {
      name: '',
      shortname: '',
      children: []
    };
  }

  addChild(node: FormGroup, element: EditableExpandableItem<Category>): void {
    (node.controls.children as FormArray).push(this.buildForm());
    if (element.dataSource) {
      // Force tree to update.
      (element.dataSource.data as any) = null;
      element.dataSource.data = [element.form];
    }
    if (element.treeControl) {
      element.treeControl.expand(node);
    }
  }

  removeCategory(node: FormGroup, element: EditableExpandableItem<Category>): void {
    const parent = node.parent as FormArray;
    const indexToRemove = parent.controls.findIndex(f => f === node);
    parent.removeAt(indexToRemove);
    parent.markAsDirty();
    if (element.dataSource) {
      // Force tree to update.
      (element.dataSource.data as any) = null;
      element.dataSource.data = [element.form];
    }
  }

  /**
   * Override the default injection behavior due to the category tree structure.
   */
  protected nbItemsInjection(stats: ItemStatsCollection[]): void {
    const rootStats = stats.reduce((acc, stat) => {
      const rootId = this.getRootParentId(stat._id);
      if (rootId) {
        if (!(rootId in acc)) {
          acc[rootId] = 0;
        }
        acc[rootId] += stat.count;
      }
      return acc;
    }, {} as { [key: string]: number });
    this.resources.forEach(item => {
      item.nbItems = rootStats[item.data._id as string] || 0;
    });
  }

  private getRootParentId(category: ItemCategory): string | undefined {
    return category._id !== undefined ? category._id :
      category.parent && category.parent._id !== undefined ? category.parent._id :
        category.parent && category.parent.parent && category.parent.parent._id;
  }

  private createFormNode(defaultValue?: Category): FormGroup {
    return new FormGroup({
      name: new FormControl(defaultValue && defaultValue.name, [Validators.required]),
      shortname: new FormControl(defaultValue && defaultValue.shortname || ''),
      children: new FormArray((defaultValue && defaultValue.children || []).map(leaf => {
        return new FormGroup({
          name: new FormControl(leaf.name, [Validators.required]),
          shortname: new FormControl(leaf.shortname || ''),
        });
      }))
    });
  }

}
