import { ChangeDetectionStrategy, ChangeDetectorRef, Component, TemplateRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Brand, ResourcesBrandsService } from '@my-everyday-lolita/mel-shared';
import { rowAnimation } from 'src/app/features/animations/fix-mat-table-route-transition';
import { EditableExpandableItem, Item } from 'src/app/features/form/form.model';
import { ResourcesItemsCountMixin } from 'src/app/features/resources/resources-items-count.mixin';
import { ResourcesListPageMixin } from 'src/app/features/resources/resources-list-page.mixin';
import { ResourcesListPage } from 'src/app/features/resources/resources-mixins.model';
import { ResourcesService } from 'src/app/features/resources/resources.service';

export class BrandResourcesListPage extends ResourcesListPage<Brand> { }

@Component({
  templateUrl: './resources-brands.component.html',
  styleUrls: ['./resources-brands.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    rowAnimation,
  ]
})
export class ResourcesBrandsComponent extends ResourcesItemsCountMixin<Brand>(ResourcesListPageMixin<Brand>(BrandResourcesListPage)) {

  displayedColumns: string[] = ['name', 'nbItems', 'actions'];
  pageSize = localStorage.getItem('pageSize') || 10;
  pageSizeOptions = [10, 20, 50];
  resources: Item<Brand>[];
  dataSource!: MatTableDataSource<Item<Brand>>;

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('addResourceDialog', { static: true }) addResourceTemplate!: TemplateRef<any>;

  constructor(
    private activatedRoute: ActivatedRoute,
    protected cdRef: ChangeDetectorRef,
    protected service: ResourcesBrandsService,
    protected dialog: MatDialog,
    protected resourcesService: ResourcesService
  ) {
    super();

    this.resources = this.toEditableExpandableItem(JSON.parse(JSON.stringify(this.activatedRoute.snapshot.data.brands)));
  }

  buildForm(resource?: Brand): FormGroup {
    const form = super.buildForm(resource);
    form.addControl('name', new FormControl(resource && resource.name || '', [Validators.required]));
    form.addControl('shortname', new FormControl(resource && resource.shortname || ''));
    form.addControl('shop', new FormControl(resource && resource.shop || ''));
    if (resource && resource._id) {
      form.addControl('_id', new FormControl(resource._id || '', [Validators.required]));
    }
    return form;
  }

  onUpdate(item: EditableExpandableItem<Brand>, response: Brand): void {
    super.onUpdate(item, response);
    item.data = response;
    item.form.setValue({
      name: response.name,
      shortname: response.shortname,
      shop: response.shop,
      _id: response._id,
    });
    item.expanded = false;
  }

  onCreate(item: EditableExpandableItem<Brand>, response: Brand, dialogRef?: MatDialogRef<TemplateRef<any>>): void {
    if (dialogRef) {
      dialogRef.close();
    }
  }

  makeEmptyItem(): Brand {
    return {
      name: '',
      shortname: '',
      shop: '',
    };
  }

}
