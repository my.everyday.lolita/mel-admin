import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ResourcesService } from 'src/app/features/resources/resources.service';

@Component({
  templateUrl: './images-processing.component.html',
  styleUrls: ['./images-processing.component.scss']
})
export class ImagesProcessingComponent {

  nonMelImages?: number;
  downloadingImages = false;
  downloadedImages = 0;
  downloadedImagesPercentage = 0;

  configForm = new FormGroup({
    limit: new FormControl(500, [Validators.min(1), Validators.max(500)]),
    delay: new FormControl(100, [Validators.min(100)]),
  });

  constructor(
    private resourcesService: ResourcesService
  ) { }

  downloadImages(): void {
    this.downloadedImages = 0;
    this.downloadingImages = true;
    const { limit, delay } = this.configForm.value;
    this.resourcesService.downloadImages(limit, delay).subscribe({
      next: (data) => {
        this.downloadedImagesPercentage = data.step * 100 / data.total;
        this.downloadedImages += 1;
        this.nonMelImages = data.total;
      },
      complete: () => {
        this.downloadingImages = false;
      }
    });
  }

  checkNonMelImages(): void {
    const { limit, _ } = this.configForm.value;
    this.resourcesService.checkNonMelImages(limit).subscribe({
      next: result => {
        this.nonMelImages = result;
      }
    });
  }

}
