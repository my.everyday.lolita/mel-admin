import { Component } from '@angular/core';
import { ResourcesService } from 'src/app/features/resources/resources.service';

@Component({
  templateUrl: './images-size.component.html',
  styleUrls: ['./images-size.component.scss']
})
export class ImagesSizeComponent {

  imagesSize = 0;
  totalSize = 0;
  imagesSizePercentage = 0;

  constructor(
    private resourcesServoce: ResourcesService
  ) { }

  checkImagesSize(): void {
    this.imagesSize = 0;
    this.totalSize = 0;
    this.imagesSizePercentage = 0;
    this.resourcesServoce.imagesSize().subscribe({
      next: (data) => {
        this.imagesSizePercentage = data.step * 100 / data.total;
        this.imagesSize += 1;
        this.totalSize += data.size;
      },
    });
  }

}
