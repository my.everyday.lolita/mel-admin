import { Component } from '@angular/core';
import { ResourcesService } from 'src/app/features/resources/resources.service';

@Component({
  templateUrl: './unused-images.component.html',
  styleUrls: ['./unused-images.component.scss']
})
export class UnusedImagesComponent {

  unusedImages?: number;
  removingImages = false;
  removedImages = 0;
  removedImagesPercentage = 0;

  constructor(
    private resourcesService: ResourcesService
  ) { }

  checkUnusedImages(): void {
    this.resourcesService.checkUnusedImages().subscribe({
      next: result => {
        this.unusedImages = result;
      }
    });
  }

  removeUnusedImages(): void {
    this.removedImages = 0;
    this.removingImages = true;
    this.resourcesService.removeUnusedImages().subscribe({
      next: (data) => {
        this.removedImagesPercentage = data.step * 100 / data.total;
        this.removedImages += 1;
      },
      complete: () => {
        this.removingImages = false;
      }
    });
  }

}
