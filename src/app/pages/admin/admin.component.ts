import { ComponentType } from '@angular/cdk/portal';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SystemInfosService } from '@my-everyday-lolita/mel-shared';
import { SystemStatus } from 'projects/mel-shared/src/lib/system-infos/system-infos.model';
import { ImagesProcessingComponent } from './dialogs/images-processing/images-processing.component';
import { ImagesSizeComponent } from './dialogs/images-size/images-size.component';
import { UnusedImagesComponent } from './dialogs/unused-images/unused-images.component';
import * as dayjs from 'dayjs';

@Component({
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  gridColumns = '1fr 1fr';

  dialogs: { [key: string]: ComponentType<any> } = {
    unusedImages: UnusedImagesComponent,
    imagesProcessing: ImagesProcessingComponent,
    imagesSize: ImagesSizeComponent,
  };

  systemInfosForm?: FormGroup;
  systemInfosStatusOptions = [
    { value: SystemStatus.OK, label: 'Ok' },
    { value: SystemStatus.MAINTENANCE, label: 'Maintenance' },
  ];

  constructor(
    private dialog: MatDialog,
    private systemInfosService: SystemInfosService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.systemInfosService.getSystemInfos().subscribe({
      next: (systemInfos) => {
        let startDate: string | undefined;
        if (systemInfos.startDate) {
          startDate = dayjs(systemInfos.startDate).format('YYYY-MM-DDTHH:mm');
        }
        this.systemInfosForm = this.fb.group({
          status: [systemInfos.status],
          estimatedDuration: [systemInfos.estimatedDuration || undefined],
          startDate: [startDate],
        });
      }
    });
  }

  open(component: ComponentType<any>): void {
    this.dialog.open(component, { disableClose: true, minWidth: '42vw' });
  }

  updateSystemInfos(): void {
    if (this.systemInfosForm) {
      const data = JSON.parse(JSON.stringify(this.systemInfosForm.value));
      if (data.startDate) {
        data.startDate = (new Date(data.startDate)).toISOString();
      }
      this.systemInfosService.updateSystemInfos(data).subscribe({
        next: () => {
          this.snackBar.open(`System infos updated.`, 'OK', { duration: 1000 });
        }
      });
    }
  }

}
