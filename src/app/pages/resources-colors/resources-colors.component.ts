import { trigger, state, style, transition, animate } from '@angular/animations';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, TemplateRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Color, ResourcesColorsService } from '@my-everyday-lolita/mel-shared';
import { rowAnimation } from 'src/app/features/animations/fix-mat-table-route-transition';
import { EditableExpandableItem, Item } from 'src/app/features/form/form.model';
import { ResourcesItemsCountMixin } from 'src/app/features/resources/resources-items-count.mixin';
import { ResourcesListPageMixin } from 'src/app/features/resources/resources-list-page.mixin';
import { ResourcesListPage } from 'src/app/features/resources/resources-mixins.model';
import { ResourcesService } from 'src/app/features/resources/resources.service';

export class ColorResourcesListPage extends ResourcesListPage<Color> { }

@Component({
  templateUrl: './resources-colors.component.html',
  styleUrls: ['./resources-colors.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    rowAnimation,
  ]
})
export class ResourcesColorsComponent extends ResourcesItemsCountMixin<Color>(ResourcesListPageMixin<Color>(ColorResourcesListPage)) {

  displayedColumns: string[] = ['name', 'hex', 'nbItems', 'actions'];
  pageSize = localStorage.getItem('pageSize') || 10;
  pageSizeOptions = [10, 20, 50];
  resources: Item<Color>[];
  dataSource!: MatTableDataSource<Item<Color>>;

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('addResourceDialog', { static: true }) addResourceTemplate!: TemplateRef<any>;

  constructor(
    private activatedRoute: ActivatedRoute,
    protected cdRef: ChangeDetectorRef,
    protected service: ResourcesColorsService,
    protected dialog: MatDialog,
    protected resourcesService: ResourcesService
  ) {
    super();

    this.resources = this.toEditableExpandableItem(JSON.parse(JSON.stringify(this.activatedRoute.snapshot.data.colors)));
  }

  buildForm(resource?: Color): FormGroup {
    const form = super.buildForm(resource);
    form.addControl('name', new FormControl(resource && resource.name || '', [Validators.required]));
    form.addControl('hex', new FormControl(resource && resource.hex || '', [Validators.required]));
    if (resource && resource._id) {
      form.addControl('_id', new FormControl(resource._id || '', [Validators.required]));
    }
    return form;
  }

  onUpdate(item: EditableExpandableItem<Color>, response: Color): void {
    super.onUpdate(item, response);
    item.data = response;
    item.form.setValue({
      name: response.name,
      hex: response.hex,
      _id: response._id,
    });
    item.expanded = false;
  }

  onCreate(item: EditableExpandableItem<Color>, response: Color, dialogRef?: MatDialogRef<TemplateRef<any>>): void {
    if (dialogRef) {
      dialogRef.close();
    }
  }

  makeEmptyItem(): Color {
    return {
      name: '',
      hex: '',
    };
  }
}
