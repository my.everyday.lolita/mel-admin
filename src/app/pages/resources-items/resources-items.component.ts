import { AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import {
  Brand,
  Category,
  Color,
  Criterium,
  ExtendedCategory,
  Feature,
  Item,
  ItemsService,
  ItemStatus,
  ItemVariant,
  substyles,
  UserService
} from '@my-everyday-lolita/mel-shared';
import { Observable, Subject } from 'rxjs';
import { map, switchMap, takeUntil } from 'rxjs/operators';
import { uploadImage } from 'src/app/features/form/validators/upload-image.validator';
import { ResourcesService } from 'src/app/features/resources/resources.service';

@Component({
  templateUrl: './resources-items.component.html',
  styleUrls: ['./resources-items.component.scss']
})
export class ResourcesItemsComponent implements OnInit, AfterViewInit, OnDestroy {

  pageSize = 10;
  pageSizeOptions = [10, 20, 50];
  resultsLength = 0;
  criteria: Criterium[] = [];
  selectedCriteria: Criterium[] = [];
  searchForm = new FormGroup({
    criteria: new FormControl([])
  });
  items: Item[] = [];
  colors: Color[] = [];
  brands: Brand[] = [];
  features: Feature[] = [];
  categories: ExtendedCategory[] = [];
  substyles = substyles;

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild('itemFormModal', { static: true }) itemFormModal!: TemplateRef<any>;

  private unsubscriber = new Subject();

  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private router: Router,
    private itemsService: ItemsService,
    private dialog: MatDialog,
    private fb: FormBuilder,
    private resourcesService: ResourcesService
  ) { }

  ngOnInit(): void {
    this.initSearch();
    this.searchForm.controls.criteria.valueChanges.pipe(takeUntil(this.unsubscriber)).subscribe({
      next: criteria => {
        this.selectedCriteria = criteria;
        this.triggerSearch();
      }
    });
  }

  ngAfterViewInit(): void {
    this.activatedRoute.queryParams.pipe(
      switchMap(queryParams => {
        if (queryParams.criteria) {
          const criteria = JSON.parse(queryParams.criteria);
          this.selectedCriteria = criteria;
          this.resetPaging();
          setTimeout(() => {
            this.searchForm.setValue({ criteria }, { emitEvent: false });
          });
        }
        return this.search();
      }),
    ).subscribe({
      next: response => {
        this.items = response.items;
        this.resultsLength = response.total;
      }
    });
  }

  onPageChange(): void {
    this.search().subscribe({
      next: response => {
        this.items = response.items;
        this.resultsLength = response.total;
      }
    });
  }

  ngOnDestroy(): void {
    this.unsubscriber.next();
    this.unsubscriber.complete();
  }

  searchCriterias(term: string, item: Criterium): boolean {
    term = term.toLowerCase();
    return (item.displayValue && item.displayValue.toLowerCase().indexOf(term) > -1) || item.type.toLowerCase().indexOf(term) > -1;
  }

  searchFn(term: string, item: any): boolean {
    term = term.toLowerCase();
    return item && (item.name && item.name.toLowerCase().includes(term) || item.shortname && item.shortname.toLowerCase().includes(term));
  }

  addCustomBrand(term: string): Brand {
    return {
      name: term,
    };
  }

  addTag(term: string): Criterium {
    let cleanTerm = term;
    let type: any = 'keyword';
    if (term.startsWith('id:')) {
      cleanTerm = term.replace('id:', '');
      type = 'id';
    } else if (term.startsWith('user:')) {
      cleanTerm = term.replace('user:', '');
      type = 'user';
    }
    return {
      value: cleanTerm,
      displayValue: term,
      type,
    };
  }

  resetPaging(): void {
    this.paginator.pageIndex = 0;
  }

  triggerSearch(): void {
    this.router.navigate([], {
      replaceUrl: true,
      queryParams: {
        criteria: JSON.stringify(this.selectedCriteria)
      }
    });
  }

  search(): Observable<{ total: number, items: Item[] }> {
    const skip = this.paginator.pageIndex * this.paginator.pageSize;
    const limit = this.paginator.pageSize;
    return this.itemsService.findByCriteria2(this.selectedCriteria, skip, limit);
  }

  edit(item: Item, index: number): void {
    this.dialog.open(this.itemFormModal, {
      data: {
        form: this.buildForm(item.draft || item),
        item,
        index
      },
      minHeight: '70vh',
      minWidth: '60vh',
      maxHeight: '90vh',
      maxWidth: '90vh',
      disableClose: true,
    });
  }

  update(data: { form: FormGroup, item: Item, index: number }, ref: MatDialogRef<any>): void {
    const values = JSON.parse(JSON.stringify(data.form.value));
    this.cleanValues(values);
    this.itemsService.update(values).subscribe({
      next: response => {
        this.items[data.index] = response;
        ref.close();
      }
    });
  }

  validateDraft(data: { form: FormGroup, item: Item, index: number }, ref: MatDialogRef<any>): void {
    const values = JSON.parse(JSON.stringify(data.form.value));
    this.cleanValues(values);
    values.status = ItemStatus.VALIDATED;
    this.itemsService.update(values).subscribe({
      next: response => {
        this.items[data.index] = response;
        ref.close();
        this.resourcesService.drafts -= 1;
        this.search().subscribe({
          next: result => {
            this.items = result.items;
            this.resultsLength = result.total;
          }
        });
      }
    });
  }

  markAsIncomplete(data: { form: FormGroup, item: Item, index: number }, ref: MatDialogRef<any>): void {
    const values = JSON.parse(JSON.stringify(data.form.value));
    this.cleanValues(values);
    values.incomplete = true;
    this.itemsService.update(values).subscribe({
      next: response => {
        this.items[data.index] = response;
        ref.close();
        this.search().subscribe({
          next: result => {
            this.items = result.items;
            this.resultsLength = result.total;
          }
        });
      }
    });
  }

  unmarkedAsIncomplete(data: { form: FormGroup, item: Item, index: number }, ref: MatDialogRef<any>): void {
    const values = JSON.parse(JSON.stringify(data.form.value));
    this.cleanValues(values);
    values.incomplete = false;
    this.itemsService.update(values).subscribe({
      next: response => {
        this.items[data.index] = response;
        ref.close();
        this.search().subscribe({
          next: result => {
            this.items = result.items;
            this.resultsLength = result.total;
          }
        });
      }
    });
  }

  addVariant(variants: FormArray, initialValue?: ItemVariant): void {
    variants.push(this._addVariant(initialValue));
  }

  addPhoto(variantControl: FormGroup, initialValue?: string): void {
    (variantControl.get('photos') as FormArray).push(this.fb.control(initialValue || null, [uploadImage]));
  }

  removePhoto(variantControl: FormGroup, index: number): void {
    (variantControl.get('photos') as FormArray).removeAt(index);
    variantControl.markAsDirty();
  }

  removeVariant(variants: FormArray, index: number): void {
    variants.removeAt(index);
  }

  ensureEmptyPhoto(variantControl: FormGroup): void {
    const photos = variantControl.get('photos') as FormArray;
    const emptyPhotos = photos.value.filter((photo: string) => !photo);
    if (emptyPhotos.length === 0) {
      this.addPhoto(variantControl);
    }
  }

  addUserCriteria(owner: string): void {
    const criteria = this.searchForm.controls.criteria.value;
    if (!criteria.find((crit: Criterium) => crit.type === 'user' && crit.value === owner)) {
      criteria.push({
        value: owner,
        displayValue: `user:${owner}`,
        type: 'user'
      });
      this.searchForm.controls.criteria.setValue(criteria);
    }
  }

  private buildForm(item: Item): any {
    return this.fb.group({
      _id: [item._id, [Validators.required]],
      brand: [item.brand, Validators.required],
      collectionn: [item.collectionn],
      category: [item.category],
      features: [item.features],
      year: [item.year],
      japanese: [item.japanese],
      measurments: [item.measurments],
      estimatedPrice: [item.estimatedPrice],
      keywords: [item.keywords],
      substyles: [item.substyles],
      owner: [item.owner, Validators.required],
      variants: this.fb.array((item.variants as any[]).map((variant: any) => this._addVariant(variant)), [Validators.required]),
      status: [item.status || ItemStatus.VALIDATED, [Validators.required]],
      incomplete: [item.incomplete || false],
    });
  }

  private cleanValues(values: Item): void {
    if (values.variants) {
      for (const variant of values.variants) {
        // clean photos
        variant.photos = variant.photos.filter(photo => !!photo);
      }
    }
  }

  private _addVariant(initialValue?: ItemVariant): FormGroup {
    const variantControl = this.fb.group({
      colors: [initialValue && initialValue.colors || null, [Validators.required]],
      photos: this.fb.array([], [Validators.required])
    });
    (initialValue && initialValue.photos || [null]).forEach((photo: any) => {
      this.addPhoto(variantControl, photo);
    });
    // Add an empty photo to allow user to add a new one.
    this.ensureEmptyPhoto(variantControl);
    return variantControl;
  }

  private initSearch(): void {
    this.criteria.push({
      type: 'own',
      displayValue: 'My items',
      value: this.userService.user?.sub as string
    });
    this.criteria.push({
      type: 'drafts',
      displayValue: 'Drafts',
      value: ''
    });
    this.criteria.push({
      type: 'mel-error',
      displayValue: 'Mel Errors',
      value: ''
    });
    this.criteria.push({
      type: 'incomplete',
      displayValue: 'Incomplete',
      value: ''
    });

    this.brands = this.activatedRoute.snapshot.data.brands as Brand[];
    this.brands.forEach(brand => {
      this.criteria.push({
        type: 'brand',
        value: brand.name,
        displayValue: brand.shortname ? `${brand.name} (${brand.shortname})` : brand.name,
      });
    });
    this.colors = this.activatedRoute.snapshot.data.colors as Color[];
    this.colors.forEach(color => {
      this.criteria.push({
        type: 'color',
        value: color.name,
        displayValue: color.name,
      });
    });

    this.features = this.activatedRoute.snapshot.data.features as Feature[];
    this.features.forEach(feature => {
      this.criteria.push({
        type: 'feature',
        value: feature.name,
        displayValue: feature.name,
      });
    });

    const categories = this.activatedRoute.snapshot.data.categories as Category[];
    categories.forEach(category => {
      this.criteria.push({
        type: 'category',
        value: category.name,
        displayValue: category.shortname ? `${category.name} (${category.shortname})` : category.name,
        _lvlClass: 'lvl-0',
      });
      this.categories.push({
        ...category,
        disabled: category.children !== undefined && category.children.length > 0,
        _lvlClass: 'lvl-0',
      });
      if (category.children) {
        category.children.forEach(category2 => {
          this.criteria.push({
            type: 'category',
            value: category2.name,
            displayValue: category2.shortname ? `${category2.name} (${category2.shortname})` : category2.name,
            parents: [category.name],
            _lvlClass: 'lvl-1',
          });
          this.categories.push({
            ...category2,
            children: undefined,
            disabled: category2.children !== undefined && category2.children.length > 0,
            _lvlClass: 'lvl-1',
            parent: { ...category, children: undefined }
          });
          if (category2.children) {
            category2.children.forEach(category3 => {
              this.criteria.push({
                type: 'category',
                value: category3.name,
                displayValue: category3.shortname ? `${category3.name} (${category3.shortname})` : category3.name,
                parents: [category.name, category2.name],
                _lvlClass: 'lvl-2',
              });
              this.categories.push({
                ...category3,
                _lvlClass: 'lvl-2',
                parent: { ...category2, children: undefined }
              });
            });
          }
        });
      }
    });
  }

}
