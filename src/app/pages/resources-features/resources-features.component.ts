import { ChangeDetectionStrategy, ChangeDetectorRef, Component, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Category, Feature, ResourcesFeaturesService } from '@my-everyday-lolita/mel-shared';
import { rowAnimation } from 'src/app/features/animations/fix-mat-table-route-transition';
import { EditableExpandableItem, Item } from 'src/app/features/form/form.model';
import { ResourcesItemsCountMixin } from 'src/app/features/resources/resources-items-count.mixin';
import { ResourcesListPageMixin } from 'src/app/features/resources/resources-list-page.mixin';
import { ResourcesListPage } from 'src/app/features/resources/resources-mixins.model';
import { ResourcesService } from 'src/app/features/resources/resources.service';

export class BrandResourcesListPage extends ResourcesListPage<Feature> { }

@Component({
  templateUrl: './resources-features.component.html',
  styleUrls: ['./resources-features.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    rowAnimation,
  ]
})
export class ResourcesFeaturesComponent extends ResourcesItemsCountMixin<Feature>(ResourcesListPageMixin<Feature>(BrandResourcesListPage)) {

  displayedColumns: string[] = ['name', 'nbItems', 'actions'];
  pageSize = localStorage.getItem('pageSize') || 10;
  pageSizeOptions = [10, 20, 50];
  resources: Item<Feature>[];
  dataSource!: MatTableDataSource<Item<Feature>>;

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('addResourceDialog', { static: true }) addResourceTemplate!: TemplateRef<any>;
  categories: { name: string }[];

  constructor(
    private activatedRoute: ActivatedRoute,
    protected cdRef: ChangeDetectorRef,
    protected service: ResourcesFeaturesService,
    protected dialog: MatDialog,
    protected resourcesService: ResourcesService
  ) {
    super();

    this.categories = (this.activatedRoute.snapshot.data.categories as Category[]).map(resource => ({ name: resource.name }));

    this.resources = this.toEditableExpandableItem(JSON.parse(JSON.stringify(this.activatedRoute.snapshot.data.features)));
  }

  buildForm(resource?: Feature): FormGroup {
    const form = super.buildForm(resource);
    form.addControl('name', new FormControl(resource && resource.name || '', [Validators.required]));
    form.addControl('categories', new FormControl(resource && resource.categories || []));
    if (resource && resource._id) {
      form.addControl('_id', new FormControl(resource._id || '', [Validators.required]));
    }
    return form;
  }

  onUpdate(item: EditableExpandableItem<Feature>, response: Feature): void {
    super.onUpdate(item, response);
    item.data = response;
    item.form.setValue({
      name: response.name,
      categories: response.categories,
      _id: response._id,
    });
    item.expanded = false;
  }

  onCreate(item: EditableExpandableItem<Feature>, response: Feature, dialogRef?: MatDialogRef<TemplateRef<any>>): void {
    if (dialogRef) {
      dialogRef.close();
    }
  }

  compareWith(o1: any, o2: any): boolean {
    if (o1.name !== undefined && o2.name !== undefined) {
      return o1.name === o2.name;
    }
    return false;
  }

  makeEmptyItem(): Feature {
    return {
      name: '',
      categories: [],
    };
  }

}
