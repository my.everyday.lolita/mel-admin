import { trigger, transition, style, query, animateChild, animate, group } from '@angular/animations';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDrawerMode } from '@angular/material/sidenav';
import { RouterOutlet, Routes } from '@angular/router';
import { SystemInfosService, User, UserService, UserSignInService } from '@my-everyday-lolita/mel-shared';
import * as FileSaver from 'file-saver';
import * as JSZip from 'jszip';
import { ToastrService } from 'ngx-toastr';
import { SystemStatus } from 'projects/mel-shared/src/lib/system-infos/system-infos.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, publish, tap } from 'rxjs/operators';
import { ResourcesService } from 'src/app/features/resources/resources.service';
import { RESOURCES_ROUTES } from './app.token';
import { SignedInModalService } from './features/user/signed-in-modal.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('routeTransition', [
      transition('* => *', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
          style({
            position: 'absolute',
            top: '32px',
            left: '32px',
            width: 'calc(100% - 64px)',
            height: '100%'
          })
        ], { optional: true }),
        query(':enter', [
          style({ transform: 'translateY(100px)', opacity: 0 })
        ], { optional: true }),
        query(':leave', animateChild(), { optional: true }),
        group([
          query(':leave', [
            animate('300ms ease-out', style({ transform: 'translateY(-100px)', opacity: 0 }))
          ], { optional: true }),
          query(':enter', [
            animate('300ms ease-out', style({ transform: 'translateY(0px)', opacity: 1 }))
          ], { optional: true })
        ]),
        query(':enter', animateChild(), { optional: true }),
      ]),
    ])
  ]
})
export class AppComponent implements OnInit {

  sidenavMode: MatDrawerMode = 'over';
  sidenavOpened = false;
  sidenavFixedInViewport = false;
  isMobile = false;
  user?: User;
  isSuperAdmin = false;
  dumpStep = '';
  dumpValue = 0;
  totalSize?: { [key: string]: number; };
  drafts$: BehaviorSubject<number>;
  draftsItemsLinkQueryParams = {
    criteria: JSON.stringify([{ type: 'drafts', displayValue: 'Drafts', value: '' }])
  };
  inMaintenance?: boolean;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private userSigneInService: UserSignInService,
    private userService: UserService,
    @Inject(RESOURCES_ROUTES) public resourcesRoutes: Routes,
    private signedInModalService: SignedInModalService,
    private toastr: ToastrService,
    private resourcesService: ResourcesService,
    private systemInfosService: SystemInfosService
  ) {
    this.drafts$ = this.resourcesService.drafts$;
    this.systemInfosService.getSystemInfos().subscribe({
      next: infos => {
        this.inMaintenance = infos.status === SystemStatus.MAINTENANCE;
      }
    });
    this.systemInfosService.listenSystemInfos().subscribe({
      next: infos => {
        this.inMaintenance = infos.status === SystemStatus.MAINTENANCE;
      }
    });
  }

  ngOnInit(): void {
    this.breakpointObserver.observe([Breakpoints.Handset, Breakpoints.Tablet, Breakpoints.Web]).subscribe({
      next: state => {
        this.isMobile = state.breakpoints[Breakpoints.HandsetLandscape] || state.breakpoints[Breakpoints.HandsetPortrait];
        this.sidenavMode = this.isMobile ? 'over' : 'side';
        this.sidenavOpened = !this.isMobile;
      }
    });
    this.userSigneInService.signedIn$.subscribe({
      next: signedIn => {
        if (signedIn) {
          this.user = this.userService.user;
          this.isSuperAdmin = this.userService.isSuperAdmin();
        } else {
          this.user = undefined;
          this.signedInModalService.open().subscribe({
            next: () => {
              this.toastr.clear();
            }
          });
        }
      }
    });

    this.resourcesService.totalSize().subscribe({
      next: totalSize => {
        this.totalSize = totalSize;
      }
    });

    publish()(this.resourcesService.countDrafts()).connect();
  }

  signOut(): void {
    this.userSigneInService.signOut();
    location.reload();
  }

  prepareRoute(outlet: RouterOutlet): any {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }

  dump(): void {
    this.dumpStep = 'starting...';
    this.dumpValue = 0;
    const zip = new JSZip();
    this.resourcesService.dump().pipe(
      tap(data => {
        this.dumpStep = `step ${data.step}/${data.total} ok!`;
        this.dumpValue = (data.step / data.total) * 100;
      }),
      map(data => {
        zip.file(`step-${data.step}-${data.total}_${data.type}.json`, JSON.stringify(data.items));
      })
    ).subscribe({
      complete: () => {
        zip.generateAsync({ type: 'blob' }).then(content => {
          FileSaver.saveAs(content, `mel-dump-${(new Date()).toLocaleDateString()}.zip`);
          this.dumpValue = 0;
          this.dumpStep = '';
        });
      }
    });
  }

}
