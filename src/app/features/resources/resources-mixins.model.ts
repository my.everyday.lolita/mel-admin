import { TemplateRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ItemsService, Resource, ResourceBaseService } from '@my-everyday-lolita/mel-shared';
import { EditableExpandableItem, EditableItem, Item } from '../form/form.model';

export class ResourcesListPage<T extends Resource> {
  buildForm(resource?: T): FormGroup {
    return new FormGroup({});
  }

  onUpdate(item: EditableItem<T>, response: T, dialogRef?: MatDialogRef<TemplateRef<any>>): void { }
  onCreate(item: EditableItem<T>, response: T, dialogRef?: MatDialogRef<TemplateRef<any>>): void { }
  makeEmptyItem(): T {
    throw new Error('You must implement the makeEmptyItem method');
  }
}

export type Constructor<T = {}> = new (...args: any[]) => T;

export interface ResourcesListPageTypeDef<T extends Resource> extends ResourcesListPage<T> {
  resultsLength: number;
  resources: Item<T>[];
  addResourceTemplate?: TemplateRef<any>;
  paginator: MatPaginator;
  dataSource: MatTableDataSource<Item<T>>;
  delete(item: Item<T>): void;
  add(): void;
  makeEmptyItem(): T;
  update(item: Item<T>, dialogRef?: MatDialogRef<TemplateRef<any>>): void;
  create(item: Item<T>, dialogRef?: MatDialogRef<TemplateRef<any>>): void;
  toEditableExpandableItem(items: T[]): EditableExpandableItem<T>[];
  resetPaging(): void;
  makeEditableExpandableItem(item: T): EditableExpandableItem<T>;
}

export interface ResourcesItemsCountTypeDef<T extends Resource> extends ResourcesListPage<T> {
  dataSource: MatTableDataSource<Item<T>>;
}
