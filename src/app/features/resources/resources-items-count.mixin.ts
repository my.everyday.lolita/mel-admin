import { AfterViewInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Resource, ResourceBaseService, ItemStatsCollection } from '@my-everyday-lolita/mel-shared';
import { Item } from '../form/form.model';
import { Constructor, ResourcesItemsCountTypeDef, ResourcesListPageTypeDef } from './resources-mixins.model';
import { ResourcesService } from './resources.service';

export function ResourcesItemsCountMixin<T extends Resource>(
  BaseClass: Constructor<ResourcesListPageTypeDef<T> & OnDestroy & AfterViewInit>
): Constructor<ResourcesListPageTypeDef<T> & OnDestroy & ResourcesItemsCountTypeDef<T> & AfterViewInit> {
  return class extends BaseClass implements ResourcesItemsCountTypeDef<T>, AfterViewInit {
    dataSource!: MatTableDataSource<Item<T>>;

    protected service!: ResourceBaseService<T>;
    protected cdRef!: ChangeDetectorRef;
    protected resourcesService!: ResourcesService;

    ngAfterViewInit(): void {
      super.ngAfterViewInit();
      this.resourcesService.stats().subscribe({
        next: (result) => {
          result.forEach(stats => {
            const resourceStats = ((stats as any)[this.service.resource] as ItemStatsCollection[]) || [];
            this.nbItemsInjection(resourceStats);
            this.cdRef.detectChanges();
          });
        }
      });
    }

    protected nbItemsInjection(stats: ItemStatsCollection[]): void {
      this.resources.forEach(item => {
        const stat = stats.find(s => s._id._id === item.data._id);
        item.nbItems = stat ? stat.count : 0;
      });
    }
  };
}
