import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ItemsService, MelSharedModuleConfig, MEL_ENV, UserSignInService, ResourcesService as MelResourcesService } from '@my-everyday-lolita/mel-shared';
import { EventSourcePolyfill } from 'event-source-polyfill';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

export interface DumpEventData {
  items: any[];
  type: string;
  step: number;
  total: number;
}

@Injectable({
  providedIn: 'root'
})
export class ResourcesService extends MelResourcesService {

  drafts$ = new BehaviorSubject<number>(0);
  socketIsOpened = false;
  private inMemoryCountDrafts = 0;

  constructor(
    @Inject(MEL_ENV) env: MelSharedModuleConfig,
    http: HttpClient,
    private userSignInService: UserSignInService,
    private itemsService: ItemsService
  ) {
    super(http, env);
  }

  set drafts(value: number) {
    this.inMemoryCountDrafts = value;
    this.drafts$.next(this.inMemoryCountDrafts);
  }

  get drafts(): number {
    return this.inMemoryCountDrafts;
  }

  /**
   * listen to the dump sse endpoint.
   */
  dump(): Observable<DumpEventData> {
    return this.createEventSource('/api/resources/dump');
  }

  totalSize(): Observable<{ [key: string]: number }> {
    return this.http.get<{ [key: string]: number }>(`${this.env.domains.mel}/api/resources/total-size`, {
      headers: new HttpHeaders({
        Authorization: 'auto'
      })
    });
  }

  countDrafts(): Observable<number> {
    return this.itemsService.countDrafts().pipe(tap(count => this.drafts = count));
  }

  downloadImages(limit = 500, delay = 100): Observable<any> {
    return this.createEventSource(`/api/super-admin/resources/items/download-images?limit=${limit}&delay=${delay}`);
  }

  imagesSize(): Observable<any> {
    return this.createEventSource('/api/super-admin/resources/items/images-size');
  }

  checkNonMelImages(limit = 500, delay = 100): Observable<number> {
    return this.http.get<number>(`${this.env.domains.mel}/api/super-admin/resources/items/check-images?limit=${limit}&delay=${delay}`, {
      headers: new HttpHeaders({
        Authorization: 'auto'
      })
    });
  }

  checkUnusedImages(): Observable<number> {
    return this.http.get<number>(`${this.env.domains.mel}/api/super-admin/resources/items/check-unused-images`, {
      headers: new HttpHeaders({
        Authorization: 'auto'
      })
    });
  }

  removeUnusedImages(): Observable<any> {
    return this.createEventSource('/api/super-admin/resources/items/remove-unused-images');
  }

  private createEventSource<T extends any>(route: string, needAuth = true): Observable<T> {
    return new Observable(obs => {
      const options: any = {};
      if (needAuth) {
        options.headers = {
          Authorization: `Bearer ${this.userSignInService.getAccessToken()}`,
        };
      }
      const stream = new EventSourcePolyfill(`${this.env.domains.mel}${route}`, options);
      stream.onmessage = event => {
        const data: DumpEventData = JSON.parse(event.data);
        obs.next(data as T);
        if (data.step === data.total) {
          stream.close();
          obs.complete();
        }
      };
      stream.onerror = (error: any) => {
        console.log(error);
        stream.close();
        obs.error(error);
      };
    });
  }
}
