import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { UserService } from '@my-everyday-lolita/mel-shared';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SignedInGuard } from './signed-in.guard';

@Injectable({
  providedIn: 'root'
})
export class SuperAdminGuard implements CanActivate {

  constructor(
    private signedInGuard: SignedInGuard,
    private userService: UserService
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const isSignedInOrObs = this.signedInGuard.canActivate(route, state);
    if (isSignedInOrObs === true) {
      return this.userService.isSuperAdmin();
    }
    return (isSignedInOrObs as Observable<boolean | UrlTree>).pipe(map(isSignedIn => {
      if (isSignedIn === true) {
        return this.userService.isSuperAdmin();
      }
      return false;
    }));
  }

}
