import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { SignInModalComponent } from './sign-in-modal/sign-in-modal.component';

@Injectable({
  providedIn: 'root'
})
export class SignedInModalService {

  ref?: MatDialogRef<SignInModalComponent, any>;

  constructor(private dialog: MatDialog) { }

  open(): Observable<any> {
    if (this.ref !== undefined) {
      return this.ref.afterClosed().pipe(tap(() => this.ref = undefined));
    }
    this.ref = this.dialog.open(SignInModalComponent, {
      disableClose: true,
    });
    return this.ref.afterClosed().pipe(tap(() => this.ref = undefined));
  }
}
