import { AbstractControl } from '@angular/forms';
import { environment } from 'src/environments/environment';

export function uploadImage(control: AbstractControl): { [key: string]: any } | null {
  if (!control.value) {
    return null;
  }
  if (!control.value.includes(environment.domains.mel)) {
    return { wrongImageLink: true };
  }
  return null;
}
