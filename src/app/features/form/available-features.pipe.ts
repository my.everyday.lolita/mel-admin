import { Pipe, PipeTransform } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Feature } from '@my-everyday-lolita/mel-shared';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Pipe({
  name: 'availableFeatures',
})
export class AvailableFeaturesPipe implements PipeTransform {

  transform(features: Feature[], control: FormControl): Observable<Feature[]> {
    return control.valueChanges.pipe(
      startWith(control.value),
      map(category => {
        return features.filter(feature => {
          const categoryNames = feature.categories.map(c => c.name);
          return category && (
            categoryNames.includes(category.name) ||
            category.parent && categoryNames.includes(category.parent.name) ||
            category.parent && category.parent.parent && categoryNames.includes(category.parent.parent.name)
          );
        });
      }));
  }

}
