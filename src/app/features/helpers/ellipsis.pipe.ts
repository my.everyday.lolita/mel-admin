import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ellipsis'
})
export class EllipsisPipe implements PipeTransform {

  transform(value: string, lenght = 42): string {
    if (value.length > lenght) {
      return `${value.slice(0, lenght)}…`;
    }
    return value;
  }

}
