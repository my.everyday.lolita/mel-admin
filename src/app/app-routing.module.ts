import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResourcesBrandsResolver, ResourcesCategoriesResolver, ResourcesColorsResolver, ResourcesFeaturesResolver } from '@my-everyday-lolita/mel-shared';
import { RESOURCES_ROUTES } from './app.token';
import { SignedInGuard } from './features/user/signed-in.guard';
import { SuperAdminGuard } from './features/user/super-admin.guard';
import { AdminComponent } from './pages/admin/admin.component';
import { HomeComponent } from './pages/home/home.component';
import { LogsComponent } from './pages/logs/logs.component';
import { ResourcesBrandsComponent } from './pages/resources-brands/resources-brands.component';
import { ResourcesCategoriesComponent } from './pages/resources-categories/resources-categories.component';
import { ResourcesColorsComponent } from './pages/resources-colors/resources-colors.component';
import { ResourcesFeaturesComponent } from './pages/resources-features/resources-features.component';
import { ResourcesItemsComponent } from './pages/resources-items/resources-items.component';

const resources: Routes = [
  {
    path: 'brands', component: ResourcesBrandsComponent, data: {
      linkLabel: 'APP.LINKS.RESOURCES.BRANDS',
      icon: 'card_membership',
      animation: 'brands',
      type: 'brands',
    },
    resolve: {
      brands: ResourcesBrandsResolver
    },
  },
  {
    path: 'categories', component: ResourcesCategoriesComponent, data: {
      linkLabel: 'APP.LINKS.RESOURCES.CATEGORIES',
      icon: 'category',
      animation: 'categories',
      type: 'categories',
    },
    resolve: {
      categories: ResourcesCategoriesResolver
    }
  },
  {
    path: 'colors', component: ResourcesColorsComponent, data: {
      linkLabel: 'APP.LINKS.RESOURCES.COLORS',
      icon: 'palette',
      animation: 'colors',
      type: 'colors',
    },
    resolve: {
      colors: ResourcesColorsResolver
    }
  },
  {
    path: 'features', component: ResourcesFeaturesComponent, data: {
      linkLabel: 'APP.LINKS.RESOURCES.FEATURES',
      icon: 'tune',
      animation: 'features',
      type: 'features',
    },
    resolve: {
      categories: ResourcesCategoriesResolver,
      features: ResourcesFeaturesResolver
    }
  },
  {
    path: 'items', component: ResourcesItemsComponent, data: {
      linkLabel: 'APP.LINKS.RESOURCES.ITEMS',
      icon: 'checkroom',
      animation: 'items',
      type: 'items',
    },
    resolve: {
      brands: ResourcesBrandsResolver,
      categories: ResourcesCategoriesResolver,
      colors: ResourcesColorsResolver,
      features: ResourcesFeaturesResolver,
    }
  },
];

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [SignedInGuard], data: { animation: 'home' } },
  {
    path: 'resources', canActivate: [SignedInGuard], children: [
      ...resources
    ]
  },
  { path: 'logs', component: LogsComponent, canActivate: [SuperAdminGuard], data: { animation: 'logs' } },
  { path: 'admin-area', component: AdminComponent, canActivate: [SuperAdminGuard], data: { animation: 'admin' } },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    { provide: RESOURCES_ROUTES, useValue: resources }
  ]
})
export class AppRoutingModule { }
