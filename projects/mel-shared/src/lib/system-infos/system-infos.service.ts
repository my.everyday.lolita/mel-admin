import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { interval, Observable, Subject } from 'rxjs';
import { map, publish, switchMap } from 'rxjs/operators';
import { MelSharedModuleConfig } from '../mel-shared.model';
import { MEL_ENV } from '../mel-shared.token';
import { SystemInfos } from './system-infos.model';

@Injectable({
  providedIn: 'root'
})
export class SystemInfosService {

  interval: Observable<SystemInfos>;
  changes = new Subject<SystemInfos>();

  constructor(
    private http: HttpClient,
    @Inject(MEL_ENV) private env: MelSharedModuleConfig,
  ) {
    this.interval = interval(60000).pipe(
      switchMap(() => this.getSystemInfos()),
    );
    publish()(this.interval).connect();
  }

  getSystemInfos(): Observable<SystemInfos> {
    return this.http.get<SystemInfos>(`${this.env.domains.mel}/api/system`).pipe(
      map(result => {
        this.changes.next(result);
        return result;
      })
    );
  }

  updateSystemInfos(systemInfos: SystemInfos): Observable<SystemInfos> {
    return this.http.put<SystemInfos>(`${this.env.domains.mel}/api/system`, systemInfos, {
      headers: new HttpHeaders({
        Authorization: 'auto'
      })
    }).pipe(
      map(result => {
        this.changes.next(result);
        return result;
      }),
    );
  }

  listenSystemInfos(): Observable<SystemInfos> {
    return this.changes.asObservable();
  }
}
