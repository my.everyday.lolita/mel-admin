export enum SystemStatus {
  MAINTENANCE = 'maintenance',
  OK = 'ok',
}

export interface SystemInfos {
  status: SystemStatus;
  estimatedDuration?: number;
  startDate?: Date;
}
