import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserSignInService } from '../../user/user-sign-in.service';

@Injectable()
export class AuthorizationInterceptor implements HttpInterceptor {

  constructor(private userSignInService: UserSignInService) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (request.headers.has('authorization')) {
      switch (request.headers.get('authorization')) {
        case 'auto':
          request = request.clone({ headers: request.headers.set('authorization', `Bearer ${this.userSignInService.getAccessToken()}`) });
          break;
        case 'if_signed':
          if (this.userSignInService.isSignedIn()) {
            request = request.clone({ headers: request.headers.set('authorization', `Bearer ${this.userSignInService.getAccessToken()}`) });
          } else {
            request = request.clone({ headers: request.headers.delete('authorization') });
          }
          break;

        default:
          break;
      }
    }
    return next.handle(request);
  }
}
