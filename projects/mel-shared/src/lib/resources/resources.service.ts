import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MelSharedModuleConfig } from '../mel-shared.model';
import { MEL_ENV } from '../mel-shared.token';
import { StatsCollection } from './resources.model';

@Injectable({ providedIn: 'root' })
export class ResourcesService {

  constructor(
    protected http: HttpClient,
    @Inject(MEL_ENV) protected env: MelSharedModuleConfig
  ) { }

  stats(): Observable<StatsCollection[]> {
    return this.http.get<StatsCollection[]>(`${this.env.domains.mel}/api/resources/stats`, {
      headers: new HttpHeaders({
        Authorization: 'auto'
      })
    });
  }

}
