import { Resource } from '../resources.model';

export interface Brand extends Resource {
  name: string;
  shortname?: string;
  shop?: string;
}
