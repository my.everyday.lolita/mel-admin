import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { UserContentClosetItem, UserContentWishlistItem } from './user-content.model';
import { UserContentService } from './user-content.service';

@Injectable({ providedIn: 'root' })
export class SharedDataResolver implements Resolve<(UserContentWishlistItem | UserContentClosetItem)[]> {

  constructor(private userContentService: UserContentService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<(UserContentClosetItem | UserContentWishlistItem)[]> {
    return this.userContentService.getSharedData(route.params.id, route.params.key);
  }

}
