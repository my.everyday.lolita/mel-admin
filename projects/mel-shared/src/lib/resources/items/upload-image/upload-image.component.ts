import { EventEmitter, HostListener, Output } from '@angular/core';
import { Component, forwardRef, HostBinding, Inject, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { MelSharedModuleConfig } from '../../../mel-shared.model';
import { MEL_ENV } from '../../../mel-shared.token';
import { ItemsService } from '../items.service';

@Component({
  selector: 'mel-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UploadImageComponent),
      multi: true,
    }
  ],
  exportAs: 'melUploadImage'
})
export class UploadImageComponent implements ControlValueAccessor {

  private val: any;
  displayValue?: SafeResourceUrl;
  width = 200;
  maxSize = 5 * 1024 * 1024;
  onChange!: (_: any) => void;
  onTouched!: () => void;

  @HostBinding('class.add')
  @Input() add = false;
  @Input() key!: string;

  @Output() imageChange = new EventEmitter<any>();

  constructor(
    private domSanitizer: DomSanitizer,
    private itemsService: ItemsService,
    @Inject(MEL_ENV) private env: MelSharedModuleConfig
  ) { }

  set value(value: any) {
    this.add = !value;
    if (value !== undefined && this.val !== value) {
      this.val = value;
      this.displayValue = this.domSanitizer.bypassSecurityTrustResourceUrl(value);
      if (this.onChange) {
        this.onChange(value);
      }
      if (this.onTouched) {
        this.onTouched();
      }
      this.imageChange.emit(this.val);
    }
  }

  get value(): any {
    return this.val;
  }

  writeValue(value: any): void {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  onImgLoad(event: Event): void {
    const target = event.composedPath()[0] as HTMLImageElement;
    this.width = target.width;
  }

  onInputChange(event: Event): void {
    if (event.target) {
      const target = event.target as HTMLInputElement;
      let file = target.files && target.files[0] || null;
      if (file && file.size >= this.maxSize) {
        target.value = '';
        file = null;
      }
      if (file) {
        this.itemsService.uploadImage(this.key, file).subscribe({
          next: response => {
            this.value = `${this.env.domains.mel}/files/${response.id}`;
          }
        });
      }
    }
  }

  @HostListener('dragover', ['$event'])
  onDragOver(event: Event): void {
    event.preventDefault();
  }

  @HostListener('drop', ['$event'])
  onDropFile(event: DragEvent): void {
    event.stopPropagation();
    event.preventDefault();
    if (event.dataTransfer) {
      if (event.dataTransfer.files.length === 1) {
        const file = event.dataTransfer.files[0];
        if (file.size >= this.maxSize) {
          return;
        }
        this.itemsService.uploadImage(this.key, file).subscribe({
          next: response => {
            this.value = `${this.env.domains.mel}/files/${response.id}`;
          }
        });
      } else {
        const fromURL = event.dataTransfer.getData('URL');
        if (!fromURL) {
          return;
        }
        this.itemsService.uploadImageFromUrl(this.key, fromURL).subscribe({
          next: response => {
            this.value = `${this.env.domains.mel}/files/${response.id}`;
          },
          error: error => {
            this.value = fromURL;
          }
        });
      }
    }
  }

}
