
export interface Resource {
  _id?: string;
}

export interface StatsCollection {
  [key: string]: any;
}
