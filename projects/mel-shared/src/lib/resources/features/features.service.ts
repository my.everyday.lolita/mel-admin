import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { MelSharedModuleConfig } from '../../mel-shared.model';
import { MEL_ENV } from '../../mel-shared.token';
import { ResourceBaseService } from '../resource-base.service';
import { Feature } from './features.model';

@Injectable({ providedIn: 'root' })
export class ResourcesFeaturesService extends ResourceBaseService<Feature> {

  resource = 'features';
  features: Feature[] = [];

  constructor(
    @Inject(MEL_ENV) protected env: MelSharedModuleConfig,
    protected http: HttpClient
  ) { super(env, http); }

  findAll(): Observable<Feature[]> {
    if (this.features.length > 0) {
      return of(this.features);
    }
    return super.findAll().pipe(
      map(features => {
        this.features = features;
        return this.features;
      })
    );
  }

}
